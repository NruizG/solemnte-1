# Stage 1 project build
FROM node:latest as builder

# Crear directorio de trabajo
WORKDIR /home/app

# Instalar Angular
RUN npm install -g @angular/cli

# Copiamos el proyecto dentro del contenedor
COPY . .

# Instalar librerias
RUN npm install

# Buildear proyecto
RUN ng build

# Stage 2 Apache server
FROM httpd:2.4

COPY --from=builder /home/app/dist/objectOriented /usr/local/apache2/htdocs/