### Problema o caracteristica

En este apartado se describe brevemente el problema o caracteristica que se debe
resolver, entregando todos los antecedentes posibles para mostrar o indicar como 
el usuario debe reproducirlo.

## Propuesta o que se espera

Se debe plantear que es lo que se espera una vex que se resuelva el issue.

## Acciones a realizar.

Se enlistan una serie de tareas en caso de existir, para ejecutar durante el 
desarrollo.