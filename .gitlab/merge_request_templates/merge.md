### Problema o característica a solucionar

Escribe brevemente cual es problema o característica que se desee implementar

### Solución

Escribe brevemente como implementaste la solución, en caso de efectuar trabajo
de frontend se recomienda integrar fotos representativas del cambio efectuado

### Como Probar

Es necesario que se establezcan las instrucciones para probar la solución desarrollada, 
por favor se lo mas detallado posible para que otros puedan reproducirlo

### Closes

Especifica los Issues que cierras al terminar este caso, recuerda que para declarar 
un cierres debes escribir **Closes #Nº** Donde Nº es el numero del issue a cerrar.