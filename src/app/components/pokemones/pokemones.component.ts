import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemones.service';
import { Blastoise } from 'src/app/classes/blastoise';
import { Pidgeot } from 'src/app/classes/pidgeot';

@Component({
  selector: 'app-pokemones',
  templateUrl: './pokemones.component.html',
  styleUrls: ['./pokemones.component.scss']
})
export class PokemonesComponent implements OnInit {
  public blastoises: Blastoise[];
  public pidgeots: Pidgeot[];

  constructor(
  private pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
    this.getBlastoise();
    this.getPidgeot();
  }

  public getBlastoise() {
    this.pokemonService.getBlastoise().subscribe(
      (blastoise) => {
         this.blastoises = blastoise.map(value => new Blastoise(value));
      }
    );
  }

  public getPidgeot() {
    this.pokemonService.getPidgeot().subscribe(
      (pidgeot) => {
        this.pidgeots = pidgeot.map(value => new Pidgeot(value));
      }
    );
  }
}
