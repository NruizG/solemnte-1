import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { RecipeService } from './services/recipe.service';
import { PokemonService} from './services/pokemones.service'
import { HttpClientModule } from '@angular/common/http';
import { PokemonesComponent } from './components/pokemones/pokemones.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    PokemonesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [PokemonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
