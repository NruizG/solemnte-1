export class Moves {
  public name: string;
  public type: string;
  public damage: number;

  constructor(data: any) {
    this.name = data.name;
    this.type = data.type;
    this.damage = data.damage;
  }

  public success(): string {
    return `${this.name} deals ${this.damage} ${this.type} damage!`;
  }

  public fail(): string {
    return `${this.name} failed!`;
  }
}