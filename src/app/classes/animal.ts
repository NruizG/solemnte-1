export class Animal {
  public name: string;
  public isMale: boolean;
  public numberOfPaws: number;

  constructor(data: any = null) {
    this.update(data);
  }

  private update(data: any): void {
    this.name = data.name;
    this.isMale = data.isMale;
    this.numberOfPaws = data.numberOfPaws;
  }

  public vivir(): string{
    return 'En animal nació';
  }
}
