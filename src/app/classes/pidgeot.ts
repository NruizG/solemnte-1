import { Pokemon } from '../classes/pokemon';

export class Pidgeot extends Pokemon {
  public numberOfWings: number;

  constructor(data: any) {
    super(data);
    this.numberOfWings = data.numberOfWings;
  }

  public fly(): string {
    return `${this.name} is flying`;
  }
}