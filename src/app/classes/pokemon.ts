import { Moves } from './moves';

export class Pokemon {
  public name: string;
  public type: string;
  public color: string;
  public url: string;
  public moves: Moves[];

  constructor(data: any = null) {
    this.update(data);
  }

  private update(data: any): void {
    this.name = data.name;
    this.type = data.type;
    this.color = data.color;
    this.url = data.url;
    this.moves = data.moves.map(value => new Moves(value));
  }

  public live(): string {
    return 'The pokemon is alive';
  }

  public eat(): string {
    return 'The pokemon is eating'
  }
  
  public sleep(): string {
    return 'The pokemon is sleeping'
  }
}

