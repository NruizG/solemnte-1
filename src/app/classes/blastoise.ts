import { Pokemon } from '../classes/pokemon';

export class Blastoise extends Pokemon {
  public numberOfClaws: number;

  constructor(data: any) {
    super(data);
    this.numberOfClaws = data.numberOfClaws;
  }

  public scratch(): string {
    return `${this.name} is scratching`;
  }

  public swim(): string {
    return `${this.name} is swimming`;
  }
}