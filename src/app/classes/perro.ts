import { Animal } from '../classes/animal';

export class Perro extends Animal {
  public raza: string;
  public color: string;

  constructor(data: any) {
    super(data);
    this.raza = data.raza;
    this.color = data.color;
  }

  public ladrar(): string {
    return "Woof!";
  }

  public getColorAndName(): string {
    return `${this.name} hace ${this.ladrar()}`;
  }
}