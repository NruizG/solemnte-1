import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class PokemonService {

  constructor(
    private http: HttpClient
  ) { }

  public getBlastoise(): any {
    return this.http.get('http://localhost:4200/assets/mocks/blastoise.json');
  }

  public getPidgeot(): any {
    return this.http.get('http://localhost:4200/assets/mocks/pidgeot.json');
  }
}