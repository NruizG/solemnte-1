import { Component, OnInit } from '@angular/core';
import { Animal } from '../classes/animal';
import { Perro } from '../classes/perro';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public gato: Animal;
  public gato2: Animal;
  public perro: any;

  constructor() { }

  public ngOnInit(): void {
    // this.gato = new Animal({
    //   name: 'cuchito',
    //   isMale: true,
    //   numberOfPaws: 4,
    // });
    // console.log(this.gato.name);
    // this.gato.name = 'Mishi';
    // console.log(this.gato.name);

    // this.perro = {
    //   name: 'cuchito',
    //   isMale: true,
    //   numberOfPaws: 4
    // };

    // this.perro = {
    //   name: 'cuchito',
    //   isMale: true,
    //   numberOfPaws: 4,
    //   numberOfTeeth: 30
    // }

    // console.log(this.perro);

    const perro = new Perro({
      name: 'Firulais',
      isMale: true,
      numberOfPaws: 4,
      raza: 'corgi',
      color: 'azul'
    })

    console.log(perro);
    console.log(perro.getColorAndName());
  }

  public vivir(): void {
    this.gato.name
  }

}
